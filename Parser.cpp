//
// Created by ilyanky on 15.05.2016.
//

#include "Parser.h"

using namespace Poseidon;


const std::string Parser::GET_REMOVE_REGEX =
        "^ *([[:alnum:]]{1,})(?: +([[:alnum:]]{2,})|)(?: +([[:alnum:]]+)|) *$";
const std::string Parser::PUT_SET_REGEX =
        "^ *([[:alnum:]]{1,})(?: +([[:alnum:]]+)|)(?: +([[:alnum:]]+)|)(?: +([[:alnum:]]+)|) *$";





std::vector<std::string> Parser::parse_get_remove(const std::string& expr, std::string* error_msg)
{
    std::regex match(GET_REMOVE_REGEX);

    std::sregex_iterator next(expr.begin(), expr.end(), match);
    std::sregex_iterator end;
    if ( next == end ) {
        *error_msg = "Undefined expression";
        return std::vector<std::string>(); // TODO: return nullptr or exception
    }

    std::smatch res = *next;

    std::string token1 = to_lower(res[1]);
    if ( !is_get_remove_token(token1)) {
        *error_msg = "Undefined query: first token not recognized";
        return std::vector<std::string>(); // TODO: return nullptr or exception
    }

    std::string token2 = to_lower(res[2]);
    if ( !is_token(token2, By)) {
        *error_msg = "Undefined query: second token not recognized";
        return std::vector<std::string>(); // TODO: return nullptr or exception
    }

    std::string arg = res[3];
    if ( arg.length() <= 0 ) {
        *error_msg = "Undefined query: '*** by' must have an argument";
        return std::vector<std::string>(); // TODO: return nullptr or exception
    }


    return {token1, token2, arg};
}


std::vector<std::string> Parser::parse_put_set(const std::string& expr, std::string* error_msg)
{
    std::regex match(PUT_SET_REGEX);

    std::sregex_iterator next(expr.begin(), expr.end(), match);
    std::sregex_iterator end;
    if ( next == end ) {
        *error_msg = "Undefined expression";
        return std::vector<std::string>(); // TODO: return nullptr or exception
    }

    std::smatch res = *next;

    std::string token1 = to_lower(res[1]);
    if ( !is_put_set_token(token1)) {
        *error_msg = "Undefined query: first token not recognized";
        return std::vector<std::string>(); // TODO: return nullptr or exception
    }

    std::string arg1 = res[2];
    if ( arg1.length() <= 0 ) {
        *error_msg = "Undefined query: missing first argument";
        return std::vector<std::string>(); // TODO: return nullptr or exception
    }

    std::string token2 = to_lower(res[3]);
    if ( !is_token(token2, By)) {
        *error_msg = "Undefined query: second token not recognized";
        return std::vector<std::string>(); // TODO: return nullptr or exception
    }

    std::string arg2 = res[4];
    if ( arg2.length() <= 0 ) {
        *error_msg = "Undefined query: missing second argument";
        return std::vector<std::string>(); // TODO: return nullptr or exception
    }


    return {token1, arg1, token2, arg2};
}


bool Parser::is_get_remove_token(const std::string& str)
{
    if ( str == get_token(Get))
        return true;
    if ( str == get_token(Remove))
        return true;

    return false;
}


std::string Parser::get_token(const Token& token)
{
    static std::map<Token, std::string> token_map = {{Get,    "get"},
                                                     {Set,    "set"},
                                                     {Put,    "put"},
                                                     {Remove, "remove"},
                                                     {As,     "as"},
                                                     {By,     "by"}};
    return token_map[token];
}


Token Parser::get_token(const std::string& token_str)
{
    static std::map<std::string, Token> token_map = {{"get",    Get},
                                                     {"set",    Set},
                                                     {"put",    Put},
                                                     {"remove", Remove},
                                                     {"as",     As},
                                                     {"by",     By}};
    return token_map[token_str];
}


bool Parser::is_token(const std::string& str, const Token& token)
{
    return str == get_token(token);

}


bool Parser::is_put_set_token(const std::string& str)
{
    if ( str == get_token(Put))
        return true;
    if ( str == get_token(Set))
        return true;

    return false;
}


Query_Stack* Parser::parse(const std::string& expr)
{
    std::string error_msg;
    std::vector<std::string> res = parse_get_remove(expr, &error_msg);
    if ( !res.empty() ) {
        Query_Stack* stack = new Query_Stack;
        stack->push(res[2]);
        stack->push(get_token(res[0]));
        return stack;
    }

    res = parse_put_set(expr, &error_msg);
    if ( !res.empty() ) {
        Query_Stack* stack = new Query_Stack;
        stack->push(res[3]);
        stack->push(res[1]);
        stack->push(get_token(res[0]));
        return stack;
    } else
        throw error_msg;
}















