//
// Created by ilyanky on 15.05.2016.
//

#include "Db.h"

using namespace Poseidon;


Variant Db::query(const std::string& query)
{
    auto stack = Parser::parse(query);
    return stack->execute();
}

