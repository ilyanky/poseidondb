#include <iostream>
#include "Tree.h"
#include "Db.h"

using namespace std;


void ParserTest()
{
    Poseidon::Parser::parse("get by 10/");
}


void to_lower_test()
{
    std::string str = "GET";
    std::string lower(str);
    std::transform(str.cbegin(), str.cend(), lower.begin(), ::tolower);
    std::cout << lower;
}


void TreeTest()
{
    Poseidon::Tree tree;

    tree.insert(3, "3");
    tree.insert(2, "2");
    tree.insert(4, "4");
    tree.insert(10, "10");

    tree.remove(10);

//    tree.debug();
//    std::cout << tree.toString() << std::endl;


    auto res = tree.find(10);
    if ( res.hasValue() )
        cout << res.to_string() << std::endl;
    else
        cout << "nothing" << std::endl;
}


void db_test()
{
    try {
        Poseidon::Db db;
        Poseidon::Variant res = db.query("get by 10");
        cout << res.to_int() << endl;
    } catch (std::string str) {
        cout << str << endl;
    }
}


Poseidon::Variant variant_test()
{
    Poseidon::Variant v("str");
    stack<Poseidon::Variant> stack;
    stack.push(v);

    Poseidon::Variant v2 = stack.top();
    stack.pop();

    return v2;
}


Poseidon::Variant get_variant_test()
{
    Poseidon::Variant v(Poseidon::Get);
    return v;
}



int main() {
    cout << "Start programm" << endl;

//    ParserTest();
//    to_lower_test();
    db_test();
//    variant_test();
//    Poseidon::Variant v = variant_test();

    cout << endl << "End programm" << endl;
    return 0;
}