//
// Created by ilyanky on 16.04.2016.
//

#ifndef POSEIDON_TREE_VARIANT_H
#define POSEIDON_TREE_VARIANT_H


#include "Utility.h"
#include <string>


namespace Poseidon {


class Variant
{

private:
    Type  m_currentType;
    void* m_data;


    void clear();

public:
    Variant();
    Variant(const Variant& other);
    Variant(const std::string& str);
    Variant(const int& number);
    Variant(const Token& token);
    ~Variant();

    Variant& operator=(const Variant& v);

    bool hasValue() const;

    Type get_type() const;

    Token to_token() const;
    std::string to_string() const;
    int to_int() const;
};



}

#endif //POSEIDONTREE_VARIANT_H
