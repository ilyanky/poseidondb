//
// Created by ilyanky on 10/16/15.
//

#include <iostream>
#include <stdexcept>
//#include <algorithm>
#include "Tree.h"

using namespace Poseidon;


Tree::Tree()
{
    m_root = nullptr;
}


// ------------ helper functions ------------
int Tree::height(const Node<Trinity, Variant>* const _node) const
{
    return _node == nullptr ? 0 : _node->height;
}
// ------------------------------------------




Node<Trinity, Variant>* Tree::_insert(Node<Trinity, Variant>* const node,
                                     std::list<Trinity>::const_iterator& key,
                                     const std::list<Trinity>::const_iterator& cend, const Variant& value) const
{
    if ( node == nullptr ) {
        auto temp_node = new Node<Trinity, Variant>(*key);
        ++key;
        if ( key == cend )
            temp_node->value = value;
        else
            temp_node->mid = _insert(temp_node->mid, key, cend, value);

        fix_height(temp_node);
        return balance(temp_node);
    }

    if ( *key < node->key )
        node->left = _insert(node->left, key, cend, value);
    else if ( *key > node->key )
        node->right = _insert(node->right, key, cend, value);
    else { // if ( node->key == *key )
        ++key;
        if ( key == cend )
            node->value = value;
        else
            node->mid = _insert(node->mid, key, cend, value);
    }

    fix_height(node);
    return balance(node);
}


//Node* const Tree::insert(Node* const subtree, Node* const key) const
//{
//    if ( subtree == nullptr )
//        return key;
//
//    if ( subtree->key == key->key ) {
//        subtree->key = key->key;
//        return subtree;
//    }
//
//    if ( key->key < subtree->key ) {
//        if ( subtree->left == nullptr || key->key <= subtree->left->maxBoundary->key )
//            subtree->left = insert(subtree->left, key);
//        else
//            subtree->mid = insert(subtree->mid, key);
//    } else { // if ( key >= subtree->key )
//        if ( subtree->right == nullptr || key->key >= subtree->right->minBoundary->key )
//            subtree->right = insert(subtree->right, key);
//        else
//            subtree->mid = insert(subtree->mid, key);
//    }
//
//
//    fix_height(subtree);
//    fixBoundaries(subtree);
//    fixCount(subtree);
////    balance(subtree);
//
//    return balance(subtree);
//}
//
//
void Tree::fix_height(OUTER Node<Trinity, Variant>* const node) const
{
    if ( node == nullptr ) {
        std::cout << "debug: fix_height node == null" << std::endl;
        return;
    }

    int left = height(node->left);
    int right = height(node->right);

    if ( left > right )
        node->height = left + 1;
    else
        node->height = right + 1;
}



int Tree::balance_factor(const Node<Trinity, Variant>* const node) const
{
    return height(node->right) - height(node->left);
}



Node<Trinity, Variant>* Tree::rotate_left(Node<Trinity, Variant>* const q) const
{
    auto p = q->right;
    q->right = p->left;
    p->left = q;

    fix_height(q);
    fix_height(p);

    return p;
}


Node<Trinity, Variant>* Tree::rotate_right(Node<Trinity, Variant>* const p) const
{
    auto q = p->left;
    p->left = q->right;
    q->right = p;

    fix_height(p);
    fix_height(q);

    return q;
}



Node<Trinity, Variant>* Tree::balance(Node<Trinity, Variant>* const node) const
{
    if( balance_factor(node) == 2 ) {
        if( balance_factor(node->right) < 0 )
            node->right = rotate_right(node->right);
        return rotate_left(node);
    }

    if( balance_factor(node)== -2 ) {
        if( balance_factor(node->left) > 0  )
            node->left = rotate_left(node->left);
        return rotate_right(node);
    }

    return node;
}



const Node<Trinity, Variant>* const Tree::_find(const Node<Trinity, Variant>* const node,
                                                const std::list<Trinity>::const_iterator& cend,
                                                OUTER std::list<Trinity>::const_iterator& key) const
{
    if ( node == nullptr )
        return nullptr;

    if ( *key < node->key )
        return _find(node->left, cend, key);
    else if ( *key > node->key )
        return _find(node->right, cend, key);
    else { // if ( node->key == *key )
        ++key;
        if ( key == cend )
            return node;
        else
            return _find(node->mid, cend, key);
    }
}


Variant Tree::find(const int& key) const
{
    auto _key = fromDecimal(key);
    auto it = _key->cbegin();
    auto cend = _key->cend();
    auto res = _find(m_root, cend, it);
    return res == nullptr ? Variant() : res->value;
}


void Tree::insert(const int& key, const std::string& value)
{
    auto _key = fromDecimal(key);
    auto it = _key->cbegin();
    auto cend = _key->cend();
    m_root = _insert(m_root, it, cend, value);
    delete _key;
}


void Tree::insert(const int& key, const int& value)
{
    auto _key = fromDecimal(key);
    auto it = _key->cbegin();
    auto cend = _key->cend();
    m_root = _insert(m_root, it, cend, value);
    delete _key;
}


void Tree::debug()
{
    // root
    std::cout << m_root->key << " " << m_root->value.hasValue() << std::endl;

//    std::cout << m_root->value.getString() << std::endl;
}


void Tree::_toString(const Node<Trinity, Variant>* const node, std::string* const result) const
{
    if ( node == nullptr )
        return;
    else {
        result->operator+=(trinityToChar(node->key));
        result->operator+=("( ");
    }

    _toString(node->left, result);
    _toString(node->mid, result);
    _toString(node->right, result);

    result->operator+=(" ) ");
}



std::string Tree::toString()
{
    std::string str;
    _toString(m_root, &str);
    return str;
}



Node<Trinity, Variant>* Tree::_remove(Node<Trinity, Variant>* const node,
                                      std::list<Trinity>::const_iterator& key,
                                      const std::list<Trinity>::const_iterator& cend) const
{
    if ( node == nullptr )
        return nullptr;

    if ( *key < node->key )
        node->left = _remove(node->left, key, cend);
    else if ( *key > node->key )
        node->right = _remove(node->right, key, cend);
    else { // if ( node->key == *key )
        ++key;
        if ( key != cend )
            node->mid = _remove(node->mid, key, cend);

        if ( node->mid != nullptr || ( key != cend && node->value.hasValue() ) ) {
            if ( key == cend ) {
                node->value = Variant();
                --key;
            }
            return node;
        }

        --key;
        auto q = node->left;
        auto r = node->right;
        delete node;
        if( r == nullptr )
            return q;
        auto min = find_min(r);
        min->right = remove_min(r);
        min->left = q;

        fix_height(min);
        return balance(min);
    }

    fix_height(node);
    return balance(node);
}


Node<Trinity, Variant>* Tree::find_min(Node<Trinity, Variant>* const node) const
{
    return node->left != nullptr ? find_min(node->left) : node;
}


Node<Trinity, Variant>* Tree::remove_min(Node<Trinity, Variant>* const node) const
{
    if( node->left == nullptr )
        return node->right;
    node->left = remove_min(node->left);
    fix_height(node);
    return balance(node);
}



void Tree::remove(const int& key)
{
    auto _key = fromDecimal(key);
    auto it = _key->cbegin();
    auto cend = _key->cend();
    m_root = _remove(m_root, it, cend);
    delete _key;
}


