//
// Created by ilyanky on 15.05.2016.
//

#ifndef POSEIDONTREE_DB_H
#define POSEIDONTREE_DB_H

#include "Tree.h"
#include "Parser.h"


namespace Poseidon {


class Db
{
private:
    Tree m_tree;

public:
    Variant query(const std::string& query);

};


}


#endif //POSEIDONTREE_DB_H
