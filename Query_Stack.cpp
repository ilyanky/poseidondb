//
// Created by ilyanky on 16.05.2016.
//

#include "Query_Stack.h"


using namespace Poseidon;

typedef Variant (Query_Stack::*Operation)(const std::vector<Variant>&) const;




void Query_Stack::push(const Variant& v)
{
    m_stack.push(v);
}


void Query_Stack::set_tree(const Tree* const tree)
{
    m_tree = tree;
}


Variant Query_Stack::execute()
{
    if ( m_stack.empty() )
        return Variant();

    Variant item = m_stack.top();
    m_stack.pop();
    if ( item.get_type() == Token_t ) {
        auto t = item.to_token();
        int count = get_arg_num(item.to_token());
        std::vector<Variant> args;
        for ( int i = 0; i < count; ++i )
            args.push_back(execute());

        return exec_operation(item.to_token(), args);
    }

    return item;
}

int Query_Stack::get_arg_num(const Token& token) const
{
    static std::map<Token, int> token_map = {{Get,    1},
                                             {Set,    2},
                                             {Put,    2},
                                             {Remove, 1},
                                             {As,     2},
                                             {By,     1}};
    return token_map[token];
}


Variant Query_Stack::exec_operation(const Token& token, const std::vector<Variant>& args) const
{
    static std::map<Token, Operation> token_map = {{Get,    &Query_Stack::get_operation},
                                                   {Set,    &Query_Stack::set_operation},
                                                   {Put,    &Query_Stack::put_operation},
                                                   {Remove, &Query_Stack::remove_operation},
                                                   {As,     &Query_Stack::as_operation},
                                                   {By,     &Query_Stack::by_operation}};

    auto found = token_map[token];
    return (this->*found)(args);
}


Variant Query_Stack::get_operation(const std::vector<Variant>& args) const
{
    return Variant(10);
}


Variant Query_Stack::set_operation(const std::vector<Variant>& args) const
{
    return Variant(10);
}


Variant Query_Stack::put_operation(const std::vector<Variant>& args) const
{
    return Variant(10);
}


Variant Query_Stack::remove_operation(const std::vector<Variant>& args) const
{
    return Variant(10);
}


Variant Query_Stack::as_operation(const std::vector<Variant>& args) const
{
    return Variant(10);
}


Variant Query_Stack::by_operation(const std::vector<Variant>& args) const
{
    return Variant(10);
}




