//
// Created by ilyanky on 16.05.2016.
//

#ifndef POSEIDONTREE_QUERY_STACK_H
#define POSEIDONTREE_QUERY_STACK_H

#include <stack>
#include <vector>
#include <map>
#include "Variant.h"
#include "Tree.h"


namespace Poseidon {


class Query_Stack
{
private:
    std::stack<Variant> m_stack;
    const Tree* m_tree;



    int get_arg_num(const Token& token) const;
    Variant exec_operation(const Token& token, const std::vector<Variant>& args) const;


    Variant get_operation(const std::vector<Variant>& args) const;
    Variant set_operation(const std::vector<Variant>& args) const;
    Variant put_operation(const std::vector<Variant>& args) const;
    Variant remove_operation(const std::vector<Variant>& args) const;
    Variant as_operation(const std::vector<Variant>& args) const;
    Variant by_operation(const std::vector<Variant>& args) const;

public:
    void push(const Variant& v);
    Variant execute();
    void set_tree(const Tree* const tree);



};


}

#endif //POSEIDONTREE_QUERY_STACK_H
