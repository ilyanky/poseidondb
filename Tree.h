//
// Created by ilyanky on 10/16/15.
//

#ifndef POSEIDON_TREE_TREE_H
#define POSEIDON_TREE_TREE_H


//#include <memory>
#include <list>
#include "Utility.h"
#include "Variant.h"


namespace Poseidon {




class Tree
{
private:
    Node<Trinity, Variant>* m_root;



    Node<Trinity, Variant>* _insert(Node<Trinity, Variant>* const node,
                          std::list<Trinity>::const_iterator& key,
                          const std::list<Trinity>::const_iterator& cend, const Variant& value) const;


    Node<Trinity, Variant>* _remove(Node<Trinity, Variant>* const node,
                                    std::list<Trinity>::const_iterator& key,
                                    const std::list<Trinity>::const_iterator& cend) const;
    Node<Trinity, Variant>* find_min(Node<Trinity, Variant>* const node) const;
    Node<Trinity, Variant>* remove_min(Node<Trinity, Variant>* const node) const;



    // ---------- helper functions
    int height(const Node<Trinity, Variant>* const _node) const;
    // --------------------------


    void fix_height(OUTER Node<Trinity, Variant>* const node) const;


    Node<Trinity, Variant>* rotate_left(Node<Trinity, Variant>* const q) const;
    Node<Trinity, Variant>* rotate_right(Node<Trinity, Variant>* const p) const;
    int                     balance_factor(const Node<Trinity, Variant>* const node) const;
    Node<Trinity, Variant>* balance(Node<Trinity, Variant>* const node) const;


    void _toString(const Node<Trinity, Variant>* const node, std::string* const result) const;

    const Node<Trinity, Variant>* const _find(const Node<Trinity, Variant>* const subtree,
                                              const std::list<Trinity>::const_iterator& cend,
                                              OUTER std::list<Trinity>::const_iterator& key) const;


public:
    Tree();

    void insert(const int& key, const std::string& value);
    void insert(const int& key, const int& value);
    void remove(const int& key);
    Variant find(const int& key) const;
//
//
    std::string toString();
    void debug();
//
//    int getBiggestSubtree(const Node* const& subtree) const;
};


}


#endif //POSEIDONTREE_TREE_H
