//
// Created by ilyanky on 16.04.2016.
//


#include "Variant.h"
#include <iostream>

using namespace Poseidon;




Variant::Variant()
{
    m_currentType = Nothing;
    m_data = nullptr;
}


Variant::Variant(const Variant& other)
{
    if ( other.m_currentType == String_t )
        m_data = new std::string(other.to_string());
    else if ( other.m_currentType == Int_t )
        m_data = new int(other.to_int());
    else if ( other.m_currentType == Token_t )
        m_data = new Token(other.to_token());
    else // if v.currentType == Nothing
        ;

    m_currentType = other.m_currentType;
}


Variant::Variant(const std::string& str)
{
    m_data = new std::string(str);
    m_currentType = String_t;
}



Variant::Variant(const int& number)
{
    m_data = new int(number);
    m_currentType = Int_t;
}



Variant::Variant(const Token& token)
{
    m_data = new Token(token);
    m_currentType = Token_t;
}



Variant::~Variant()
{
    clear();
}


void Variant::clear()
{
    if ( m_currentType == String_t )
        delete (std::string*)m_data;
    else if ( m_currentType == Int_t )
        delete (int*)m_data;
    else if ( m_currentType == Token_t )
        delete (Token*)m_data;
//    else if ( t == Trit_t )
//        delete (Trit*)m_data;
    else
        return;
}


Variant& Variant::operator=(const Variant& v)
{
    if ( this == &v )
        return *this;

    clear();
    if ( v.m_currentType == String_t )
        m_data = new std::string(v.to_string());
    else if ( v.m_currentType == Int_t )
        m_data = new int(v.to_int());
    else if ( v.m_currentType == Token_t )
        m_data = new Token(v.to_token());
    else // if v.currentType == Nothing
        ;

    m_currentType = v.m_currentType;
    return *this;
}



bool Variant::hasValue() const
{
    return m_currentType != Nothing;
}



std::string Variant::to_string() const
{
    if ( m_currentType == String_t )
        return *((std::string*)m_data);
    else
        throw std::string("Variant stores other data type");
}


int Variant::to_int() const
{
    if ( m_currentType == Int_t )
        return *((int*)m_data);
    else
        throw std::string("Variant stores other data type");
}


Type Variant::get_type() const
{
    return m_currentType;
}

Token Variant::to_token() const
{
    if ( m_currentType == Token_t )
        return *((Token*)m_data);
    else
        throw std::string("Variant stores other data type");
}







