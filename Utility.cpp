//
// Created by ilyanky on 23.04.2016.
//

#include "Utility.h"

namespace Poseidon {



Trinity toTrinity(const int& decimal)
{
    if ( decimal == 0 )
        return Z;
    else if ( decimal == 1 )
        return U;
    else if ( decimal == -1 )
        return T;
    else
        throw std::out_of_range("argument out of range");
}



void negative(std::list<Trinity>* const list)
{
    auto ii = list->begin();
    while ( ii != list->end()) {
        if ( *ii == Z ) {
            ++ii;
            continue;
        }

        if ( *ii == T )
            *ii = U;
        else
            *ii = T;

        ++ii;
    }
}



std::list<Trinity>* fromDecimal(const int& _decimal)
{
    auto res = new std::list<Trinity>();

    int temp = _decimal;
    if ( _decimal < 0 )
        temp = -_decimal;

    while ( temp >= 3 ) {
        if ( temp % 3 == 2 ) {
            temp = temp / 3 + 1;
            res->push_front(T);
        } else {
            res->push_front(toTrinity(temp % 3));
            temp = temp / 3;
        }
    }
    if ( temp != 2 )
        res->push_front(toTrinity(temp));
    else {
        res->push_front(T);
        res->push_front(U);
    }

    if ( _decimal < 0 ) {
        negative(res);
        return res;
    }
    else
        return res;
}



char trinityToChar(const Trinity& tr)
{
    if ( tr == Z )
        return '0';
    else if ( tr == U )
        return '1';
    else
        return 'T';
}


std::string to_lower(const std::string& str)
{
    std::string lower(str);
    std::transform(str.cbegin(), str.cend(), lower.begin(), ::tolower);
    return lower;
}


}