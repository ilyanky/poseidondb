//
// Created by ilyanky on 15.05.2016.
//

#ifndef POSEIDONTREE_PARSER_H
#define POSEIDONTREE_PARSER_H


#include <string>
#include <regex>
#include <algorithm>
#include <map>
#include <vector>
#include <list>
#include "Variant.h"
#include "Query_Stack.h"


namespace Poseidon {


class Parser
{
private:
    static const std::string GET_REMOVE_REGEX;
    static const std::string PUT_SET_REGEX;


    static bool is_get_remove_token(const std::string& str);
    static bool is_put_set_token(const std::string& str);
    static bool is_token(const std::string& str, const Token& token);
    static std::string get_token(const Token& token);
    static Token get_token(const std::string& token_str);
    static std::vector<std::string> parse_get_remove(const std::string& expr, std::string* error_msg);
    static std::vector<std::string> parse_put_set(const std::string& expr, std::string* error_msg);

public:
    static Query_Stack* parse(const std::string& expr);



};


}

#endif //POSEIDONTREE_PARSER_H
