//
// Created by ilyanky on 16.04.2016.
//

#ifndef POSEIDONTREE_UTILITY_H
#define POSEIDONTREE_UTILITY_H

#define OUTER


#include <list>
#include <stdexcept>
#include <algorithm>


namespace Poseidon {


enum Type
{
    Trinity_t,
    Trit_t,
    String_t,
    Int_t,
    Token_t,
    Nothing
};


enum Token
{
    Get,
    Set,
    Put,
    Remove,
    As,
    By
};


/*
 * Enum for ternary values
  *-------------------------------------
 */
enum Trinity
{
    T, // -1 or T
    Z, //  0
    U  //  1
};


/*
 * Node struct for tree
 *-------------------------------------
 */
template<typename Key_type, typename Value_type>
struct Node
{
    Node* left;
    Node* right;
    Node* mid;

    int height;

    Key_type key;
    Value_type value;

    Node()
    {
        left = nullptr;
        right = nullptr;
        mid = nullptr;

        height = 1;

        key = Z;
    }


    Node(Key_type _key, Value_type _value) : Node()
    {
        key = _key;
        value = value;
    }


    Node(Key_type _key) : Node()
    {
        key = _key;
    }
};


/*
 * Functions for convert decimal to trinity sequence
 *-------------------------------------
 */
Trinity toTrinity(const int& decimal);

void negative(std::list<Trinity>* const list);

std::list<Trinity>* fromDecimal(const int& _decimal);

char trinityToChar(const Trinity& tr);

std::string to_lower(const std::string& str);


}

#endif //POSEIDONTREE_UTILITY_H
